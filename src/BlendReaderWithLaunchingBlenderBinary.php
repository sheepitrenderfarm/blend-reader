<?php

/**
 * Copyright (C) 2013-2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace SheepItRenderFarm\BlendReader;

use Psr\Log\LoggerInterface;

/**
 * Get data information about a blender.
 * Support Blender 3.0 and up
 */
class BlendReaderWithLaunchingBlenderBinary implements BlendReader {
    private string $SCRIPT = __DIR__ . "/pyscript.py";
    private string $PRE_LOAD_SCRIPT = __DIR__ . "/pre_load_script_disable_viewport.py";
    
    private LoggerInterface $logger;
    private ?string $path;
    private string $blenderBinary;

    private array|bool|null $data;
    
    public function __construct(LoggerInterface $logger, string $b30x) {
        $this->logger = $logger;
        $this->blenderBinary = $b30x;
        $this->data = null;
        $this->path = null;
    }
    
    public function open(string $path): bool {
        if (file_exists($path) == false) {
            $this->logger->error('BlendReader::open failed to get data from file '.$path);
            return false;
        }

        $this->path = $path;
        return true;
    }

    public function getVersion(): string {
        if (is_null($this->data)) {
            $this->data = $this->getInfos();
        }

        if (is_bool($this->data)) {
            return ''; // failure is an empty string
        }

        // data['version'] is like "blender300", must return "300"

        if (strlen($this->data['version']) > strlen('blender')) {
            return substr($this->data['version'], strlen('blender'));
        }

        return $this->data['version'];
    }

    public function getInfos(): array|bool {
        $output_file = tempnam(sys_get_temp_dir(), "out");
        $script_path = tempnam(sys_get_temp_dir(), "rea");
        $pre_load_script_path = tempnam(sys_get_temp_dir(), "pre");

        $path = str_replace(array(' ', '(', ')', "'"), array('\ ', '\(', '\)', '\\\''), $this->path);

        copy($this->SCRIPT, $script_path);
        copy($this->PRE_LOAD_SCRIPT, $pre_load_script_path);

        $command = "OUTPUT_FILE=$output_file BLEND_FILE=$path ".$this->blenderBinary." --threads 4 --factory-startup --disable-autoexec -b -P $pre_load_script_path -P $script_path $path 2>&1";

        $output_command = shell_exec($command);
        $contents = file_get_contents($output_file);
        @unlink($output_file);
        @unlink($script_path);
        @unlink($pre_load_script_path);

        if ($contents === false) {
            $this->logger->error(__method__.' failed to get data from file '.$this->path.' exists? '.serialize(file_exists($this->path)).' readable? '.serialize(is_readable($path)));

            return false;
        }

        $ret = json_decode($contents, true);

        if (is_array($ret)) {
            if (array_key_exists('can_use_tile', $ret) == false) {
                $ret['can_use_tile'] = false;
            }
            else {
                $ret['can_use_tile'] = ($ret['can_use_tile'] == 'True');
            }

            $ret['supported_Color_Management'] = stripos($output_command, 'Color management:') === false;
        }
        elseif (is_null($ret)) {
            $ret = false; // do not return null but a bool
        }

        return $ret;
    }
}
