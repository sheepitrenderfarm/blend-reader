import bpy
import sys
from bpy.app.handlers import persistent
@persistent
def disable_viewport_visibility(hide_dummy):
	print('PRE_LOAD_SCRIPT_hide_viewport')
	#Hide collections in the viewport
	for col in bpy.data.collections:
		col.hide_viewport = True
	for obj in bpy.data.objects:
		#Don't hide objects in the viewport because the blend reder times out if too many objects are in the blend
		#obj.hide_viewport = True
		#Hide modifier in the viewport
		for mod in obj.modifiers:
			mod.show_viewport = False
	sys.stdout.flush()
bpy.app.handlers.version_update.append(disable_viewport_visibility)
